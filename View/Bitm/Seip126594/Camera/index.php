<?php
echo"List page";
include_once '../../../../vendor/autoload.php';
use App\Bitm\Seip126594\Camera\Camera;


?>
<br/>
<a href="../../../../index.php">Back to Home</a>
<br/>
<a href="create.php">Add New</a>
<br>
Download:<a href="pdf.php">Pdf</a> | <a href="xl.php">Xl</a><br/>
<?php

$obj= new Camera();
$Alldata=$obj->index();


if(isset($_SESSION['delmgs']) && !empty($_SESSION['delmgs'])){
    echo $_SESSION['delmgs'];
    unset($_SESSION['delmgs']);
}
?>
<br/>
<table border="1">
    <tr>
        <th>SL</th>
        <th>Id</th>
        <th>Camera Brand</th>
        <th>Model</th>
        <th>Offer</th>
        <th>Price</th>
        <th>Weber</th>
        <th>Total Price</th>
        <th colspan="3">Action</th>
        
    </tr>
    
   <?php
   $serial=1;
   if(isset($Alldata) && !empty($Alldata)){
       foreach ($Alldata as $single){?>
     <tr>
        <td><?php echo $serial++ ?></td>
        <td><?php echo $single['id'] ?></td>
        <td><?php echo $single['title'] ?></td>
        <td><?php echo $single['model'] ?></td>
        <td><?php echo $single['offer'] ?></td>
        <td><?php echo $single['cost'] ?></td>
        <td><?php echo $single['weber'] ?></td>
        <td><?php echo $single['total'] ?></td>
        <td><a href="show.php?id=<?php echo $single['unique_id']?>">View</a></td>
        <td><a href="edit.php?id=<?php echo $single['unique_id']?>">Edit</a></td>
        <td><a href="delete.php?id=<?php echo $single['unique_id']?>">Delete</a></td>
    </tr>
   <?php }
  
    
    }
    else { ?>
    <tr>
        <td colspan="11">
            No Available Data;
        </td>
    </tr>
    <?php }?>
</table>