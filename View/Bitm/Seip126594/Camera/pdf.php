<?php
include '../../../../vendor/mpdf/mpdf/mpdf.php';
include_once '../../../../vendor/autoload.php';
use App\Bitm\Seip126594\Camera\Camera;

$obj= new Camera();
$Alldata= $obj->index();
$trs=' ';
$serial=0;

foreach($Alldata as $data):
    $serial++;
    $trs.="<tr>";
    $trs.="<td>".$serial."</td>";
    $trs.="<td>".$data['id']."</td>";
    $trs.="<td>".$data['unique_id']."</td>";
    $trs.="<td>".$data['title']."</td>";
    $trs.="<td>".$data['model']."</td>";
    $trs.="<td>".$data['offer']."</td>";
    $trs.="<td>".$data['cost']."</td>";
    $trs.="<td>".$data['weber']."</td>";
    $trs.="<td>".$data['total']."</td>";
    $trs.="</tr>";
endforeach;


$html = <<<EOD
          <!doctype html>
                   <html>
                   <head>
                             <title>List Of Camera</title>
                   </head>
                   <body style="background-color:Green;">
                             <h1 style="color:blue";> List Of Camera Price</h1>
                                      <table border="1">
                             <thead>
                                      <tr>
                                         <th>Sl</th>
                                         <th>Id</th>
                                         <th>Unique_ID</th>
                                         <th>Camera Brand</th>
                                         <th>Model</th>
                                         <th>Offer</th>
                                         <th>Price</th>
                                         <th>Weber</th>
                                         <th>Total Price</th>
        
                                         
                                      </tr>
                             </thead>
                             <tbody>
                                      $trs;
                             </tbody>
                
             </table>
          </body>
            
        </html>
EOD;

$mpdf = new mPDF();
$mpdf->WriteHTML($html);
$mpdf->Output();
exit;

?>

