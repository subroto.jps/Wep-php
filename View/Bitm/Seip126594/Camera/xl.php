<?php
error_reporting(E_ALL);
error_reporting(E_ALL & ~E_DEPRECATED);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE );
date_default_timezone_set('Europe/London');

if (PHP_SAPI == 'cli')
    die('This example should only be run from a web Browser');

/** Include PHPExcel */
require_once '../../../../vendor/phpoffice/phpexcel/Classes/PHPExcel.php';
require_once '../../../../vendor/autoload.php';
use App\Bitm\Seip126594\Camera\Camera;;
$obj = new Camera();
$alldata=$obj->index();

//Create nev PHPExcel object
$objPHPExcel = new PHPExcel();
//set document properties
$objPHPExcel->getProperties()->setCreator("Marrten Balliauw")
        ->setLastModifiedBy("Marrten Balliauw")
        ->setTitle("Office 2007 XLSX Test Document")
        ->setSubject("Office 2007 XLSX Test Document")
        ->setDescription("Test Document for Office 2007 XLSX, generated using PHP classes.")
        ->setKeywords("office 2007 openxml php")
        ->setCategory("Test result file");
// add some data
$objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', 'SL')
        ->setCellValue('B1', 'ID')
        ->setCellValue('C1', 'Unique_ID')
        ->setCellValue('D1', 'Title')
        ->setCellValue('E1', 'Model')
        ->setCellValue('F1', 'Offer')
        ->setCellValue('G1', 'Price')
        ->setCellValue('H1', 'Weber')
        ->setCellValue('I1', 'Total Price');
$counter = 2;
$serial=0;

foreach ($alldata as $data){
    $serial++;
    $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A' .$counter, $serial)
            ->setCellValue('B' .$counter, $data['id'])
            ->setCellValue('C' .$counter, $data['unique_id'])
            ->setCellValue('D'.$counter, $data['title'])
            ->setCellValue('E'.$counter, $data['model'])
            ->setCellValue('F'.$counter, $data['offer'])
             ->setCellValue('G'.$counter, $data['cost'])
             ->setCellValue('H'.$counter, $data['weber'])
             ->setCellValue('I'.$counter, $data['total']);
     $counter++;
}

// Rename worksheet 
$objPHPExcel->getActiveSheet()->setTitle('Book_List');

//Set active sheet index

$objPHPExcel->setActiveSheetIndex(0);


header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="01simple.xls"');
header('Cache-Control: max-age=0');
//
header('Cache-Control: max-age=1');
   //     
header('Expires: Mon, 26 Jul 1995 05:00:00 GMT ');               
header('Last-modified: '. gmdate('D, d M Y:i:s').' GMT');
header('Cache-Control: cache, must-revalidate');
header('Pragma: public');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter ->Save('php://output');
exit;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

