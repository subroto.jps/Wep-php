<?php

include_once '../../../../vendor/autoload.php';
use App\Bitm\Seip126594\Camera\Camera;

$editobj= new Camera();
$editobj->prepare($_GET);
$single= $editobj->show();

if(isset($_SESSION['updmgs']) && !empty($_SESSION['updmgs'])){
    echo $_SESSION['updmgs'];
    unset($_SESSION['updmgs']);
}

if(isset($single) && !empty($single)){
?>

<html>
    <head>
        <title>Add Page</title>
    </head>
    <body>
        <fieldset>
            <legend>Camera List</legend>
            <form action="update.php" method="post">
                <label>Camera Brand</label>
                <input type="text" name="Camera_Brand" value="<?php
                 echo $single['title']?>"
                 />
                <br/>
                <label>Camera Model</label>
                <select type="select" name="Camera_Model">
                    <option>M1</option>
                    <option>M2</option>
                    <option>M3</option>
                </select>
                <br/>
                <Label>Offer</label>
                <input type="checkbox" name="Offer"/>
                <br/>
                <input type="hidden" name="id" value="<?php echo $_GET['id'] ?>"/>
                <br/>
                <input type="submit" value="Submitted"/>
                
                <br/>
                <a href="index.php">back to list</a>
                        

            </form>
        </fieldset>
    </body>
</html>
 <?php } else{
     $_SESSION['Err_Msg']="Error data";
     header('location:error.php');
 }?>

